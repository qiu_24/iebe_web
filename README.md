=== Run Server ===
Go to web_interface foler, run:
python manage runserver 0.0.0.0:8000
Makes sure you have django and numpy installed.
The server can be accessed locally on localhost:8000.

=== Make the port external ===
Make changes using iptable. If use service like koding.com or nitrous.io, the port is forwarded automatically.

=== Add database ===
Add .db file to web_interface/databases folder. The server will automatically pick it up.
